# Training DevOps 201902 #

Setting Koneksi Database Local

* Username : `bukutamu`
* Password : `bukutamu123`
* Host Server : `localhost`
* Nama Database : `bukutamudb`

Cara membuat username database di localhost

```
grant all on bukutamudb.* to bukutamu@localhost identified by 'bukutamu123';
```

Cara membuat database di localhost

```
create database bukutamudb;
```

Cara menjalankan aplikasi

```
mvn spring-boot:run
```

## Deployment Pivotal ##

* Username : trainingdevops201902@yopmail.com
* Password : JA9zUWt1dg-

1. Create database MySQL dari layanan ClearDB menggunakan paket Spark yang gratis

    ```
    cf create-service cleardb spark aplikasi-bukutamu-db
    ``` 

2. Bind database ke aplikasi

    ```
    cf bs aplikasi-bukutamu aplikasi-bukutamu-db
    ```

3. Build aplikasi

    ```
    mvn clean package -DskipTests
    ```

4. Deploy Aplikasi

    ```
    cf push
    ```

5. Pantau log aplikasi untuk mengantisipasi bila ada error

    ```
    cf logs aplikasi-bukutamu
    ```

6. Bila lancar, browse ke halaman aplikasi [aplikasi-bukutamu-fantastic-okapi.cfapps.io/](aplikasi-bukutamu-fantastic-okapi.cfapps.io/)