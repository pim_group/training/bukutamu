create table tamu (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nama VARCHAR(255) not null,
  email VARCHAR(100) not null,
  no_hp VARCHAR(100) not null,
  alamat VARCHAR(255)
);
