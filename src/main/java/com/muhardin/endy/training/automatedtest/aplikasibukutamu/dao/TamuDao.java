package com.muhardin.endy.training.automatedtest.aplikasibukutamu.dao;

import com.muhardin.endy.training.automatedtest.aplikasibukutamu.entity.Tamu;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TamuDao extends PagingAndSortingRepository<Tamu, Integer>{
    List<Tamu> findTamuByNamaOrderByNama(String nama);
}
